package org.tapost.ws.shop;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("org.tapost.ws")
@IncludeTags(MoneyTest.NAME)
public class MoneyTestPlan {
}
