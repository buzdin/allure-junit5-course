package org.tapost.ws.shop;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tapost.ws.sut.Account;
import org.tapost.ws.sut.InternetShop;
import org.tapost.ws.sut.Item;
import org.tapost.ws.sut.ShoppingCart;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class InternetShopTests {

    private static final Logger logger = LoggerFactory.getLogger("TEST");

    @BeforeEach
    public void setUp() {
        InternetShop.login("admin", "admin");
    }

    @AfterEach
    public void tearDown() {
        InternetShop.logout();
    }

    @Test
    public void listsAllItems() {
        List<Item> items = InternetShop.listItems();

        assertEquals(3, items.size());
        Item item = items.get(0);
        assertAll("item",
                () -> assertEquals("Ice Cream", item.getName()),
                () -> assertEquals("Tasty Ice Cream", item.getDescription()),
                () -> assertEquals(new BigDecimal("10"), item.getPrice())
        );

        logger.info("All fine!");
    }

    @ParameterizedTest(name = "[{index}] item with id {0}")
    @ValueSource(strings = {"1", "2", "3"})
    public void viewItem(Integer itemId) {
        Item item = InternetShop.viewItem(itemId);
        assertNotNull(item);
    }

    @ParameterizedTest
    @MethodSource("dataItems")
    public void fullData(Integer itemId, String name, @ConvertWith(BigDecimalConverter.class) BigDecimal price) {
        Item item = InternetShop.viewItem(itemId);
        assertAll("item",
                () -> assertEquals(name, item.getName()),
                () -> assertEquals(price, item.getPrice())
        );
    }

    public static Stream<Arguments> dataItems() {
        return Stream.of(
                Arguments.of(1, "Ice Cream", "10"),
                Arguments.of(2, "Donut", "5")
        );
    }

    @Nested
    class CheckoutTests {

        @BeforeEach
        public void setUp() {
            InternetShop.topUpBalance(new BigDecimal(100));
        }

        @MoneyTest
        @DisplayName("Buy and checkout from the store")
        public void buyAndCheckout() {
            InternetShop.addToCart(1, 10);
            ShoppingCart shoppingCart = InternetShop.viewShoppingCart();
            assertEquals(new BigDecimal(100), shoppingCart.getTotal());
            assertEquals(1, shoppingCart.getItems().size());
            assertEquals(new Integer(10), shoppingCart.getItems().iterator().next().getAmount());

            String result = InternetShop.checkout("my address");
            assertEquals("OK", result);
        }

        @Test
        @Disabled("was failing from time to time")
        public void brokenTest() {
            throw new NullPointerException();
        }

        @Nested
        class NegativeTests {

            @Test
            public void noMoney() {
                assertThrows(IllegalArgumentException.class,
                        () -> InternetShop.addToCart(1, 100000));
            }

        }

    }

    @Test
    public void test() {
        System.out.println("I am a test case");

        List<Item> items = InternetShop.listItems();
        System.out.println(items);

        Account account = InternetShop.login("admin", "admin");
        System.out.println(account);

        InternetShop.topUpBalance(new BigDecimal(1.0));

        System.out.println(InternetShop.balance());

        List<Item> found = InternetShop.searchItems("Ice");
        InternetShop.addToCart(found.iterator().next().getId(), 10);
        ShoppingCart shoppingCart = InternetShop.viewShoppingCart();
        System.out.println(shoppingCart);

        String status = InternetShop.checkout("home address");
        System.out.println(status);

        System.out.println(account);

        System.out.println(InternetShop.balance());

        InternetShop.logout();
    }

}
